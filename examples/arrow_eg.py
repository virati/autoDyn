#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#%%
"""
Created on Sat May  8 19:07:49 2021

@author: virati
Arrow animation example with Lorentz
"""


import sys
sys.path.append('/home/virati/Dropbox/projects/Research/Control/autoDyn/lib/')

#from dynSys import rk_integrator
from dynSys import dsys, brain_net

